/*
 * Visualiseur InstrumenPoche en Javascript et SVG
 * @author Association Sésamath
 * @licence AGPL-3.0-or-later
 */
'use strict'
// core-js ne contient que les polyfill du langage js, pas ceux des specs web, faut ajouter ça nous-même
import 'whatwg-fetch/dist/fetch.umd'

import iepLoad from './iepLoad.module'

export default iepLoad
